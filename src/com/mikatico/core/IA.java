package com.mikatico.core;

import com.mikatico.view.Cell;
import com.mikatico.view.Grid;
import javafx.util.Pair;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class IA extends Player {

    private static final String IA_NAME = "IA";

    public IA() {
        super(Coloring.IA, IA_NAME);
    }

    public void play() {
        final IA.Play play = new IA.Play();

        //On veut que l'ia joue les coin en premier,
        // on initialise une liste des positions jouable puis on effectue un random pour obtenir l'index

        play.playPosition();
    }

    private class Play {

        private final Game game;
        private final Grid grid;
        private final List<Cell> cells;
        private final List<Color> positions;

        private List<Integer> cornerPosition;
        private final List<Integer> positionPlayable;

        private int rand;

        private Play() {
            this.game = Game.getInstance();
            this.grid = game.getGrid();
            this.cells = grid.getCells();
            this.positions = game.getPositionsPlayed();

            positionPlayable = new ArrayList<>();

            for (int i = 0; i < positions.size(); i++) {
                if (positions.get(i).equals(Color.WHITE)) {
                    positionPlayable.add(i);
                }
            }

            this.cornerPosition = Arrays.asList(0, 2, 6, 8);
        }

        /**
         * 0 1 2
         * 3 4 5
         * 6 7 8
         */
        private void playPosition() {
            try {
                //je vérifie de pouvoir jouer les coins
                final List<Integer> cornerPlayable = new ArrayList<>();
                for (Integer playable : positionPlayable) {
                    if (cornerPosition.contains(playable)) {
                        cornerPlayable.add(playable);
                    }
                }

                boolean playCorner = false;
                if (cornerPlayable.size() >= 3) {
                    playCorner = true;
                } else {
                    int countMyCorner = 0;
                    for (Integer integer : cornerPosition) {
                        if (positions.get(integer).equals(IA.this.getPlayerColor())) {
                            countMyCorner++;
                        }
                    }
                    if (countMyCorner <= 1) {
                        playCorner = new Random().nextBoolean();
                    }
                }

                //je veux jouer les coins en premier si possible
                if (playCorner) {
                    rand = (int) (Math.random() * cornerPlayable.size());
                    game.addPositionPlayed(cells.get(cornerPlayable.get(rand)));
                } else {
                    // j'essai de jouer entre les coins
                    List<Pair<Integer, Integer>> combos = Arrays.asList(
                            new Pair<>(0, 6),
                            new Pair<>(1, 7),
                            new Pair<>(2, 8),
                            new Pair<>(0, 2),
                            new Pair<>(3, 5),
                            new Pair<>(6, 8),
                            new Pair<>(2, 6),
                            new Pair<>(0, 8)
                    );

                    //Si je peux gagner
                    for (Pair<Integer, Integer> combo : combos) {
                        if ((positions.get(combo.getKey()).equals(IA.this.getPlayerColor())
                                && positions.get(combo.getValue()).equals(IA.this.getPlayerColor()))) {
                            int position = ((combo.getKey() + combo.getValue()) / 2);

                            if (positionPlayable.contains(position)) {
                                game.addPositionPlayed(cells.get(position));
                                return;
                            }
                        }
                    }

                    //si je dois empêcher de gagner
                    for (Pair<Integer, Integer> combo : combos) {
                        if ((positions.get(combo.getKey()).equals(Coloring.GREEN.getColor())
                                && positions.get(combo.getValue()).equals(Coloring.GREEN.getColor()))) {
                            int position = ((combo.getKey() + combo.getValue()) / 2);

                            if (positionPlayable.contains(position)) {
                                game.addPositionPlayed(cells.get(position));
                                return;
                            }
                        }
                    }

                    //sinon je joue ce qu'il reste
                    rand = (int) (Math.random() * positionPlayable.size());
                    game.addPositionPlayed(cells.get(positionPlayable.get(rand)));


                }
            } catch (Exception e) {
                e.getStackTrace();
                playPosition();
            }

        }
    }
}
