package com.mikatico.core;

import java.awt.*;

public class Player {

    public enum Coloring {
        GREEN(Color.GREEN),
        RED(Color.RED),
        IA(Color.ORANGE);


        private Color color;

        Coloring(Color color) {
            this.color = color;
        }

        public Color getColor() {
            return color;
        }
    }

    private String name;

    private Color playerColor;

    Player(Coloring coloring, String name) {
        this.playerColor = coloring.getColor();
        this.name = name;
    }

    String getName() {
        return this.name;
    }

    Color getPlayerColor() {
        return this.playerColor;
    }

    public boolean isWin() {
        return false;
    }
}
