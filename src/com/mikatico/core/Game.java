package com.mikatico.core;

import com.mikatico.view.Cell;
import com.mikatico.view.Grid;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Game {

    private static Game game;

    public static synchronized Game getInstance() {
        if (game == null) {
            game = new Game();
        }
        return game;
    }

    private Player playerOne;

    private Player playerTwo;

    private Player currentPlayer;

    private final List<Color> positionsPlayed;

    private final Grid grid;

    private boolean partyEnd = false;

    private Game() {
        grid = new Grid();

        positionsPlayed = new ArrayList<>(
                Arrays.asList(
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE
                )
        );

        System.out.println(" Cells : " + positionsPlayed.size());

        playerOne = new Player(Player.Coloring.GREEN, "John");
        playerTwo = new Player(Player.Coloring.RED, "Doe");

        currentPlayer = playerOne;
        grid.setPlayerName(currentPlayer.getName());
    }

    private Game(IA ia) {
        grid = new Grid();

        positionsPlayed = new ArrayList<>(
                Arrays.asList(
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE,
                        Color.WHITE
                )
        );

        System.out.println(" Cells : " + positionsPlayed.size());

        playerOne = new Player(Player.Coloring.GREEN, "John");
        playerTwo = ia;

        currentPlayer = playerOne;
        grid.setPlayerName(currentPlayer.getName());
    }

    public static void newInstance() {
        if (game != null) {
            game.getGrid().clearContent();
        }
        game = new Game();
    }

    public static void newInstanceWithIA() {
        if (game != null) {
            game.getGrid().clearContent();
        }
        game = new Game(new IA());
    }

    private void endTurn() {
        if (currentPlayer == playerOne) {
            currentPlayer = playerTwo;
        } else if (currentPlayer == playerTwo) {
            currentPlayer = playerOne;
        } else {
            throw new IllegalStateException("Player must be initialized");
        }
        grid.setPlayerName(currentPlayer.getName());
        if (currentPlayer instanceof IA) {
            ((IA) currentPlayer).play();
        }
    }

    public void addPositionPlayed(Cell cell) {
        if (!partyEnd) {
            if (currentPlayer == null) {
                throw new IllegalStateException("Player must be initialized");
            }
            final int position = cell.getPosition();
            if (!positionsPlayed.get(position).equals(Color.WHITE)) {
                System.out.println("Already played");
                return;
            }
            cell.setBackground(currentPlayer.getPlayerColor());
            positionsPlayed.set(position, currentPlayer.getPlayerColor());
            isGameEnd();
        }
    }

    private boolean playerWin() {
        final Color color = currentPlayer.getPlayerColor();
        if (color == positionsPlayed.get(0) && color == positionsPlayed.get(1) && color == positionsPlayed.get(2)) {
            return true;
        } else if (color == positionsPlayed.get(3) && color == positionsPlayed.get(4) && color == positionsPlayed.get(5)) {
            return true;
        } else if (color == positionsPlayed.get(6) && color == positionsPlayed.get(7) && color == positionsPlayed.get(8)) {
            return true;
        } else if (color == positionsPlayed.get(0) && color == positionsPlayed.get(3) && color == positionsPlayed.get(6)) {
            return true;
        } else if (color == positionsPlayed.get(1) && color == positionsPlayed.get(4) && color == positionsPlayed.get(7)) {
            return true;
        } else if (color == positionsPlayed.get(2) && color == positionsPlayed.get(5) && color == positionsPlayed.get(8)) {
            return true;
        } else if (color == positionsPlayed.get(0) && color == positionsPlayed.get(4) && color == positionsPlayed.get(8)) {
            return true;
        } else
            return color == positionsPlayed.get(2) && color == positionsPlayed.get(4) && color == positionsPlayed.get(6);
    }

    private void isGameEnd() {
        if (playerWin()) {
            grid.showdialog(Grid.DIALOG_WIN + currentPlayer.getName());
            partyEnd = true;
        } else {
            boolean isEndGame = true;
            for (Color color : positionsPlayed) {
                if (color == Color.white) {
                    isEndGame = false;
                    break;
                }
            }
            if (isEndGame) {
                partyEnd = true;
                grid.showdialog(Grid.DIALOG_NUL);
            } else {
                endTurn();
            }
        }
    }

    List<Color> getPositionsPlayed() {
        return positionsPlayed;
    }

    Grid getGrid() {
        return grid;
    }
}
