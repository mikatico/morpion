package com.mikatico.view;

import com.mikatico.core.Game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.ArrayList;

public class Grid extends JFrame {

    private static final String APP_NAME = "Morpion";
    private static final String MENU_GAME = "Game";
    private static final String MENU_HELP = "Help";
    private static final String TEXT_NEW_GAME = "New game";
    private static final String TEXT_NEW_GAME_IA = "New game vs IA";
    private static final String TEXT_EXIT = "Exit";
    private static final String TEXT_ABOUT = "About";
    private static final int SCREEN_WIDTH = 300;
    private static final int SCREEN_HEIGHT = 300;
    private static final int GRID_ROW = 3;
    private static final int GRID_COLUMN = 3;

    private static final String DIALOG_ABOUT = "Ceci est un Morpion codé en Java :) ";
    public static final String DIALOG_WIN = "Partie terminé, le vainqueur est : ";
    public static final String DIALOG_NUL = "Partie terminé, aucun gagnant";

    private final List<Cell> cells;

    public Grid() {
        this.cells = new ArrayList<>();

        this.setTitle(APP_NAME);
        this.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setResizable(false);

        drawMenu();
        drawGrid();

        //Always called last
        this.setVisible(true);
    }

    private void drawMenu() {
        JMenuBar menuBar = new JMenuBar();

        JMenu menuGame = new JMenu(MENU_GAME);

        JMenuItem menuItemNewGame = new JMenuItem(new AbstractAction(TEXT_NEW_GAME) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Game.newInstance();
            }
        });

        JMenuItem menuItemNewGameWithIA = new JMenuItem(new AbstractAction(TEXT_NEW_GAME_IA) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Game .newInstanceWithIA();
            }
        });

        JMenuItem menuItemExit =  new JMenuItem(new AbstractAction(TEXT_EXIT) {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        menuGame.add(menuItemNewGame);
        menuGame.add(menuItemNewGameWithIA);
        menuGame.addSeparator();
        menuGame.add(menuItemExit);

        menuBar.add(menuGame);

        JMenu menuHelp = new JMenu(MENU_HELP);

        JMenuItem menuItemAbout = new JMenuItem(new AbstractAction(TEXT_ABOUT) {
            @Override
            public void actionPerformed(ActionEvent e) {
                Grid.this.showdialog(DIALOG_ABOUT);
            }
        });

        menuHelp.add(menuItemAbout);

        menuBar.add(menuHelp);

        setJMenuBar(menuBar);

    }

    private void drawGrid() {
        GridLayout grid = new GridLayout(GRID_ROW, GRID_COLUMN);
        this.setLayout(grid);
        int index = 0;
        for (int i = 0; i < GRID_ROW; i++) {
            for (int j = 0; j < GRID_COLUMN; j++) {
                Cell cell = new Cell(index);
                cells.add(index, cell);
                this.getContentPane().add(cell);
                index ++;
            }
        }
        this.setVisible(true);
    }

    public void clearContent() {
        this.getContentPane().removeAll();
        this.repaint();
        this.setVisible(false);
        this.dispose();
    }

    public void showdialog(String message) {
        JOptionPane.showMessageDialog(Grid.this, message);

    }

    public void setPlayerName(String name) {
        this.setTitle(APP_NAME + " - " + name);
    }

    public List<Cell> getCells() {
        return cells;
    }

}
